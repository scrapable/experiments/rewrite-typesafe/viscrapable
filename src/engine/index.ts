import { sleep } from "../utils"

export class CDPCommand {
	readonly commandName: string
	readonly args: any
}

export class ScrapableEvent {
	readonly op: string
	readonly args: any
	readonly delay: number
}

export type TransportFn = (command: CDPCommand) => Promise<void>

const createEngine = (dispatch: TransportFn) => ({
	execute: async (evt: ScrapableEvent) => {
		await sleep(evt.delay)
		switch(evt.op) {
			case "goto":
				await dispatch({
					commandName: "Page.navigate",
					args: {
						url: evt.args.url
					}
				})
		}
	}
})

export default createEngine
