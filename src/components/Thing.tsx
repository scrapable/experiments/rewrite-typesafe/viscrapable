import * as React from "react"

const Thing = () => {
	return (
		<div>
			<p>Other component</p>
			<webview
				id="webview"
				src="https://t0ast.cc"
				style={{
					width: 500,
					height: 300,
				}}>
			</webview>
		</div>
	)
}
 
export default Thing
