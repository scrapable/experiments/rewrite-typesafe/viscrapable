import * as React from "react"
import { Component } from "react"
import { hot } from "react-hot-loader"
import Thing from "./Thing"
import { isElectron } from "../utils"
import createEngine, { CDPCommand } from "../engine"

const doThing = async () => {
	if(!isElectron) return
	const webview = document.getElementById("webview")
	webview.addEventListener("dom-ready", async () => {
		const dbugger = (webview as any).getWebContents().debugger
		dbugger.attach()
		const transportToDebugger = (command: CDPCommand) =>
			dbugger.sendCommand(command.commandName, command.args)
		const engine = createEngine(transportToDebugger)
		await engine.execute({
			op: "goto",
			args: {
				url: "https://t0ast.cc/about"
			},
			delay: 500
		})
		await engine.execute({
			op: "goto",
			args: {
				url: "https://t0ast.cc/blog"
			},
			delay: 2000
		})
	}, { once: true })
}

class App extends Component {
	state = { counter: 0 }

	render() {
		const { counter } = this.state
		const setCounter = counter => this.setState({ counter })

		return (
			<div>
				<h1>Hello world from {isElectron ? "Electron" : "browser"}</h1>
				<p>Counter is {counter}</p>
				<button onClick={() => setCounter(counter+1)}>Inc</button>
				<Thing />
			</div>
		)
	}

	componentDidMount() {
		doThing()
	}
}

export default hot(module)(App)
