import { app, BrowserWindow, ipcMain, BrowserView, Debugger } from "electron"
import * as path from "path"
import { isDev } from "../utils"

let mainWindow: BrowserWindow | null = null

const createWindow = async () => {
	mainWindow = new BrowserWindow({
		width: 900,
		height: 680,
		webPreferences: {
			webviewTag: true,
		}
	})

	if(isDev) {
		mainWindow.loadURL("http://localhost:1234")
	} else {
		mainWindow.loadFile(path.resolve(__dirname, "./app/index.html"))
	}
	mainWindow.on("closed", () => (mainWindow = null))
}

app.on("ready", createWindow)

app.on("window-all-closed", () => {
	if(process.platform !== "darwin") {
		app.quit()
	}
})

app.on("activate", () => {
	if(mainWindow === null) {
		createWindow()
	}
})
