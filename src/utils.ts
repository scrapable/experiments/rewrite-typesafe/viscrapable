export const isDev = process.env.NODE_ENV === "dev"

export const isElectron = (() => {
    // Renderer process
    if (typeof window !== "undefined" && typeof window.process === "object" && window.process.type === "renderer") {
        return true
    }

    // Main process
    if (typeof process !== "undefined" && typeof process.versions === "object" && !!process.versions.electron) {
        return true
    }

    // Detect the user agent when the `nodeIntegration` option is set to true
    if (typeof navigator === "object" && typeof navigator.userAgent === "string" && navigator.userAgent.indexOf("Electron") >= 0) {
        return true
    }

    return false;
})()

export const sleep = timeout =>
    new Promise(resolve =>
            setTimeout(resolve, timeout))
